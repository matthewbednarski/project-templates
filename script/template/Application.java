import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Slf4jReporter;
import config.Server;
import org.aeonbits.owner.ConfigCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static spark.Spark.*;

class Application {
    final Logger LOG = LoggerFactory.getLogger(Application.class);
    final MetricRegistry metrics = new MetricRegistry();

    public static void main(String[] args) {
        Application application = new Application();
        application.bootstrap(args);
    }
    private void bootstrap(String[] args){
        LOG.info("" + args.length);
        Server config = ConfigCache.getOrCreate(Server.class,
                (Map)System.getProperties().clone(),
                System.getenv()
        );

        Slf4jReporter reporter = Slf4jReporter.forRegistry(metrics)
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .build();
        reporter.start(10, TimeUnit.SECONDS);
        Meter meter = metrics.meter("requests");

        port(config.port());
        get("/config", (req, res) -> {
            Map map = new HashMap();
            config.fill(map);
            return map;
        });
        path( "api", () -> {
            before("/*", (req, res) -> {
                meter.mark();
            });
            get("/hello", (req, res) -> {
                return "Hello World";
            });
        });
    }
}
