package config;

import org.aeonbits.owner.Config.Sources;
import org.aeonbits.owner.Config.LoadType;
import org.aeonbits.owner.Config.LoadPolicy;
import org.aeonbits.owner.Accessible;
import org.aeonbits.owner.Mutable;
import org.aeonbits.owner.Reloadable;

@LoadPolicy(LoadType.MERGE)
@Sources({
        "file:${server.name}.properties",
        "file:application.properties",
        "file:${HOME}/${server.name}.properties",
        "file:/etc/${server.name}.properties",
        "classpath:${server.name}.properties",
        "classpath:application.properties",
        "classpath:config/Server.properties"
})
public interface Server extends Accessible, Mutable, Reloadable {

    @Key( "server.port" )
    @DefaultValue("8080")
    int port();

    @Key( "server.request.timeout" )
    @DefaultValue( "90000" )
    int requestTimeout();

    @Key( "server.read.timeout" )
    @DefaultValue( "60000" )
    int readTimeout();

    @Key( "server.connection.timeout" )
    @DefaultValue( "10000" )
    int connectionTimeout();
}
